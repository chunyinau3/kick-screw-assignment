# Kickscrew Take Home Assignment



## Getting started

| Java  | 11  |
|-------|-----|
| Junit | 5   |

## Procedure 
1. Check out source code 
2. build project using Maven
3. run text by `mvn test`


in the project there is two comparison method one is sorting the array(s) first doing comparison one by one.
The time Complexity should be O(n log n). 

Second one is using hashing We store all elements of first array and their counts in a hash table.
Then we traverse second array and check if the count of every element in second array matches with the count 
in first array. The time Complexity should be O(n).