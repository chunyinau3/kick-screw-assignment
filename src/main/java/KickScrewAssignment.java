import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class KickScrewAssignment {

    public static void main(String[] args){
    }


    public static boolean isEqual(Integer[] arrayA, Integer[] arrayB) {
        if (arrayA.length != arrayB.length) {
            return false;
        }

        int[] sortedArrayA = Arrays.stream(arrayA)
                .sorted(Comparator.naturalOrder())
                .mapToInt(Integer::intValue)
                .toArray();

        int[] sortedArrayB = Arrays.stream(arrayB)
                .sorted(Comparator.naturalOrder())
                .mapToInt(Integer::intValue)
                .toArray();

        for (int i = 0; i < sortedArrayA.length; i ++) {
            if (sortedArrayA[i] != sortedArrayB[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean isEqualByHashSearch(Integer[] arrayA, Integer[] arrayB) {
        if (arrayA.length != arrayB.length) {
            return false;
        }
        Map<Integer, Integer> map = new HashMap<>();
        int count = 0;
        for (int i = 0; i < arrayA.length; i++) {
            if (map.get(arrayA[i]) == null) {
                map.put(arrayA[i], 1);
            } else {
                count = map.get(arrayA[i]);
                count++;
                map.put(arrayA[i], count);
            }
        }
        for (int i = 0; i < arrayB.length; i++) {
            if (!map.containsKey(arrayB[i]) ||
                    map.get(arrayB[i]) == 0) {
                return false;
            }
            count = map.get(arrayB[i]);
            --count;
            map.put(arrayB[i], count);
        }
        return true;
    }
}
