import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class KickScrewAssignmentTest {

    @Test
    public void should_test_success() {
        Integer[] arrayA = new Integer[] {1, 2, 3, 3};
        Integer[] arrayB = new Integer[] {3, 1, 2, 3};
        Assertions.assertTrue(KickScrewAssignment.isEqual(arrayA, arrayB));
        Assertions.assertTrue(KickScrewAssignment.isEqualByHashSearch(arrayA, arrayB));
    }

    @Test
    public void should_test_success_2() {
        Integer[] arrayA = new Integer[] {1, 2, 5, 4, 0, 2, 1};
        Integer[] arrayB = new Integer[] {2, 4, 5, 0, 1, 1, 2};
        Assertions.assertTrue(KickScrewAssignment.isEqual(arrayA, arrayB));
        Assertions.assertTrue(KickScrewAssignment.isEqualByHashSearch(arrayA, arrayB));
    }

    @Test
    public void should_test_fail() {
        Integer[] arrayA = new Integer[] {0, 0, 1};
        Integer[] arrayB =new Integer[] {0, 1};
        Assertions.assertFalse(KickScrewAssignment.isEqual(arrayA, arrayB));
        Assertions.assertFalse(KickScrewAssignment.isEqualByHashSearch(arrayA, arrayB));
    }

    @Test
    public void should_test_fail_2() {
        Integer[] arrayA = new Integer[] {1, 2, 5, 4, 0, 2, 1, 3};
        Integer[] arrayB = new Integer[] {2, 4, 5, 0, 1, 1, 2, 6};
        Assertions.assertFalse(KickScrewAssignment.isEqual(arrayA, arrayB));
        Assertions.assertFalse(KickScrewAssignment.isEqualByHashSearch(arrayA, arrayB));
    }
}
